export default [
    {
        "ch": "Planete+ HD",
        "acc": "JM",
        "value": 1
    },
    {
        "ch": "Ale kino+ HD",
        "acc": "N",
        "value": 13
    },
    {
        "ch": "teleTOON+ HD",
        "acc": "AD",
        "value": 16
    },
    {
        "ch": "CANAL+ DOMO HD",
        "acc": "AD",
        "value": 4
    },
    {
        "ch": "CANAL+ DOMO HD",
        "acc": "N",
        "value": 6
    },
    {
        "ch": "MiniMini+ HD",
        "acc": "JM",
        "value": 9
    },
    {
        "ch": "MiniMini+ HD",
        "acc": "N",
        "value": 23
    },
    {
        "ch": "TV 6",
        "acc": "AD",
        "value": 2
    },
    {
        "ch": "TV 6",
        "acc": "JM",
        "value": 3
    },
    {
        "ch": "POLSAT News HD",
        "acc": "JM",
        "value": 2
    },
    {
        "ch": "MTV Polska HD",
        "acc": "N",
        "value": 18
    },
    {
        "ch": "POLSAT Games",
        "acc": "JM",
        "value": 2
    },
    {
        "ch": "POLSAT Games",
        "acc": "N",
        "value": 11
    },
    {
        "ch": "PULS 2",
        "acc": "AD",
        "value": 3
    },
    {
        "ch": "PULS 2",
        "acc": "N",
        "value": 7
    },
    {
        "ch": "AXN Spin HD",
        "acc": "N",
        "value": 24
    },
    {
        "ch": "TV Puls",
        "acc": "AD",
        "value": 2
    },
    {
        "ch": "Cartoon Network HD",
        "acc": "N",
        "value": 32
    },
    {
        "ch": "TV 4",
        "acc": "AD",
        "value": 2
    },
    {
        "ch": "POLSAT Sport Fight HD",
        "acc": "N",
        "value": 4
    },
    {
        "ch": "POLSAT Comedy Central Extra HD",
        "acc": "N",
        "value": 1
    },
    {
        "ch": "POLSAT Sport Fight HD",
        "acc": "AD",
        "value": 3
    },
    {
        "ch": "POLSAT",
        "acc": "N",
        "value": 7
    },
    {
        "ch": "POLSAT",
        "acc": "AD",
        "value": 1
    },
    {
        "ch": "POLSAT Film HD",
        "acc": "AD",
        "value": 8
    },
    {
        "ch": "TV Republika",
        "acc": "N",
        "value": 1
    },
    {
        "ch": "POLSAT Sport Extra HD",
        "acc": "N",
        "value": 1
    },
    {
        "ch": "CANAL+ Family HD",
        "acc": "JM",
        "value": 1
    },
    {
        "ch": "CANAL+ Family HD",
        "acc": "AD",
        "value": 5
    },
    {
        "ch": "TV 4",
        "acc": "JM",
        "value": 4
    },
    {
        "ch": "CANAL+ Family HD",
        "acc": "N",
        "value": 8
    },
    {
        "ch": "Cinemax2 HD",
        "acc": "N",
        "value": 13
    },
    {
        "ch": "SAT.1",
        "acc": "N",
        "value": 6
    },
    {
        "ch": "TVN Style HD",
        "acc": "AD",
        "value": 5
    },
    {
        "ch": "TVN Style HD",
        "acc": "N",
        "value": 10
    },
    {
        "ch": "Antena TV",
        "acc": "N",
        "value": 5
    },
    {
        "ch": "Eska TV Extra",
        "acc": "N",
        "value": 2
    },
    {
        "ch": "WP",
        "acc": "N",
        "value": 2
    },
    {
        "ch": "HBO3 HD",
        "acc": "N",
        "value": 21
    },
    {
        "ch": "TV 4",
        "acc": "N",
        "value": 12
    },
    {
        "ch": "Super Polsat",
        "acc": "JM",
        "value": 4
    },
    {
        "ch": "Biznes24 HD",
        "acc": "N",
        "value": 4
    },
    {
        "ch": "Super Polsat",
        "acc": "N",
        "value": 16
    },
    {
        "ch": "Super Polsat",
        "acc": "AD",
        "value": 7
    },
    {
        "ch": "ZOOM TV",
        "acc": "N",
        "value": 1
    },
    {
        "ch": "Eska TV",
        "acc": "JM",
        "value": 1
    },
    {
        "ch": "Cinemax HD",
        "acc": "N",
        "value": 13
    },
    {
        "ch": "TVN",
        "acc": "AD",
        "value": 2
    },
    {
        "ch": "TVR",
        "acc": "AD",
        "value": 2
    },
    {
        "ch": "TVN",
        "acc": "N",
        "value": 10
    },
    {
        "ch": "TVR",
        "acc": "N",
        "value": 6
    },
    {
        "ch": "CANAL+ KUCHNIA HD",
        "acc": "N",
        "value": 2
    },
    {
        "ch": "CANAL+ 1",
        "acc": "JM",
        "value": 1
    },
    {
        "ch": "CANAL+ PREMIUM HD",
        "acc": "N",
        "value": 14
    },
    {
        "ch": "CANAL+ Seriale HD",
        "acc": "N",
        "value": 23
    },
    {
        "ch": "CANAL+ PREMIUM HD",
        "acc": "AD",
        "value": 3
    },
    {
        "ch": "teleTOON+ HD",
        "acc": "N",
        "value": 15
    },
    {
        "ch": "TVN 7",
        "acc": "N",
        "value": 9
    },
    {
        "ch": "Stopklatka TV",
        "acc": "N",
        "value": 10
    },
    {
        "ch": "TV Trwam",
        "acc": "JM",
        "value": 9
    },
    {
        "ch": "POLSAT News HD",
        "acc": "N",
        "value": 13
    },
    {
        "ch": "HBO2 HD",
        "acc": "N",
        "value": 17
    },
    {
        "ch": "nSport+ HD",
        "acc": "N",
        "value": 4
    },
    {
        "ch": "TVN Turbo HD",
        "acc": "AD",
        "value": 2
    },
    {
        "ch": "Eska TV",
        "acc": "N",
        "value": 4
    },
    {
        "ch": "TVN 24 HD",
        "acc": "JM",
        "value": 2
    },
    {
        "ch": "TVN 24 HD",
        "acc": "N",
        "value": 11
    },
    {
        "ch": "TVR",
        "acc": "JM",
        "value": 2
    },
    {
        "ch": "wPolsce.pl",
        "acc": "JM",
        "value": 2
    },
    {
        "ch": "teleTOON+ HD",
        "acc": "JM",
        "value": 21
    },
    {
        "ch": "POLSAT Play HD",
        "acc": "AD",
        "value": 17
    },
    {
        "ch": "CANAL+ Sport 2 HD",
        "acc": "N",
        "value": 5
    },
    {
        "ch": "TNT HD",
        "acc": "N",
        "value": 1
    },
    {
        "ch": "TVN Fabuła HD",
        "acc": "AD",
        "value": 1
    },
    {
        "ch": "Discovery Life",
        "acc": "N",
        "value": 5
    },
    {
        "ch": "TV Trwam",
        "acc": "AD",
        "value": 1
    },
    {
        "ch": "POLSAT Sport News HD",
        "acc": "AD",
        "value": 2
    },
    {
        "ch": "Biznes24 HD",
        "acc": "JM",
        "value": 2
    },
    {
        "ch": "POLSAT Seriale",
        "acc": "N",
        "value": 25
    },
    {
        "ch": "POLSAT Café HD",
        "acc": "N",
        "value": 7
    },
    {
        "ch": "POLSAT Rodzina",
        "acc": "N",
        "value": 7
    },
    {
        "ch": "POLSAT Rodzina",
        "acc": "AD",
        "value": 8
    },
    {
        "ch": "TVP Dokument",
        "acc": "AD",
        "value": 1
    },
    {
        "ch": "POLSAT Sport Premium 2",
        "acc": "JM",
        "value": 1
    },
    {
        "ch": "Antena TV",
        "acc": "JM",
        "value": 2
    },
    {
        "ch": "TTV HD",
        "acc": "N",
        "value": 13
    },
    {
        "ch": "POLSAT Sport Premium 2",
        "acc": "AD",
        "value": 1
    },
    {
        "ch": "POLSAT Film HD",
        "acc": "JM",
        "value": 1
    },
    {
        "ch": "CANAL+ Sport 3",
        "acc": "AD",
        "value": 1
    },
    {
        "ch": "POLSAT Film HD",
        "acc": "N",
        "value": 11
    },
    {
        "ch": "POLSAT Games",
        "acc": "AD",
        "value": 2
    },
    {
        "ch": "CANAL+ Discovery",
        "acc": "AD",
        "value": 5
    },
    {
        "ch": "Kino Polska Muzyka",
        "acc": "N",
        "value": 2
    },
    {
        "ch": "CANAL+ Sport 3",
        "acc": "N",
        "value": 2
    },
    {
        "ch": "POLSAT Sport Premium 1",
        "acc": "JM",
        "value": 1
    },
    {
        "ch": "CANAL+ Sport 4",
        "acc": "N",
        "value": 1
    },
    {
        "ch": "POLSAT Seriale",
        "acc": "AD",
        "value": 9
    },
    {
        "ch": "TVP Kobieta",
        "acc": "AD",
        "value": 1
    },
    {
        "ch": "CANAL+ KUCHNIA HD",
        "acc": "AD",
        "value": 3
    },
    {
        "ch": "TVP Polonia",
        "acc": "AD",
        "value": 1
    },
    {
        "ch": "StudioMED TV",
        "acc": "N",
        "value": 5
    },
    {
        "ch": "Planete+ HD",
        "acc": "AD",
        "value": 2
    },
    {
        "ch": "Cartoon Network HD",
        "acc": "AD",
        "value": 30
    },
    {
        "ch": "TVP Info",
        "acc": "JM",
        "value": 7
    },
    {
        "ch": "TVP Dokument",
        "acc": "N",
        "value": 6
    },
    {
        "ch": "TVP Kobieta",
        "acc": "N",
        "value": 5
    },
    {
        "ch": "CANAL+ 1",
        "acc": "N",
        "value": 13
    },
    {
        "ch": "TVP Seriale",
        "acc": "AD",
        "value": 8
    },
    {
        "ch": "CANAL+ 4K Ultra HD",
        "acc": "N",
        "value": 1
    },
    {
        "ch": "TVS",
        "acc": "JM",
        "value": 1
    },
    {
        "ch": "Antena TV",
        "acc": "AD",
        "value": 2
    },
    {
        "ch": "TVP Rozrywka",
        "acc": "N",
        "value": 12
    },
    {
        "ch": "TVP Historia",
        "acc": "JM",
        "value": 4
    },
    {
        "ch": "POLSAT 2",
        "acc": "JM",
        "value": 2
    },
    {
        "ch": "POLSAT Sport News HD",
        "acc": "N",
        "value": 3
    },
    {
        "ch": "TVP Historia",
        "acc": "AD",
        "value": 1
    },
    {
        "ch": "CANAL+ Sport 2 HD",
        "acc": "AD",
        "value": 1
    },
    {
        "ch": "Nicktoons",
        "acc": "N",
        "value": 1
    },
    {
        "ch": "TVP Kultura",
        "acc": "N",
        "value": 8
    },
    {
        "ch": "POLSAT Sport Premium 1",
        "acc": "AD",
        "value": 1
    },
    {
        "ch": "POLSAT 2",
        "acc": "N",
        "value": 19
    },
    {
        "ch": "TVP HD",
        "acc": "AD",
        "value": 9
    },
    {
        "ch": "POLSAT Play HD",
        "acc": "N",
        "value": 13
    },
    {
        "ch": "CANAL+ DOMO HD",
        "acc": "JM",
        "value": 2
    },
    {
        "ch": "CANAL+ Discovery",
        "acc": "N",
        "value": 15
    },
    {
        "ch": "TVP HD",
        "acc": "N",
        "value": 19
    },
    {
        "ch": "TV Trwam",
        "acc": "N",
        "value": 16
    },
    {
        "ch": "TVN Fabuła HD",
        "acc": "N",
        "value": 4
    },
    {
        "ch": "TVP Polonia",
        "acc": "N",
        "value": 16
    },
    {
        "ch": "wPolsce.pl",
        "acc": "N",
        "value": 4
    },
    {
        "ch": "TTV HD",
        "acc": "AD",
        "value": 5
    },
    {
        "ch": "TVP ABC",
        "acc": "AD",
        "value": 9
    },
    {
        "ch": "Planete+ HD",
        "acc": "N",
        "value": 2
    },
    {
        "ch": "TVP 3",
        "acc": "N",
        "value": 4
    },
    {
        "ch": "CANAL+ 1",
        "acc": "AD",
        "value": 3
    },
    {
        "ch": "TVP Info",
        "acc": "N",
        "value": 13
    },
    {
        "ch": "CBS Europa HD",
        "acc": "N",
        "value": 1
    },
    {
        "ch": "METRO",
        "acc": "AD",
        "value": 1
    },
    {
        "ch": "STARS.TV",
        "acc": "N",
        "value": 2
    },
    {
        "ch": "TVP 2",
        "acc": "AD",
        "value": 1
    },
    {
        "ch": "CANAL+ PREMIUM HD",
        "acc": "JM",
        "value": 1
    },
    {
        "ch": "Disco Polo Music",
        "acc": "N",
        "value": 7
    },
    {
        "ch": "TVP 1",
        "acc": "N",
        "value": 17
    },
    {
        "ch": "HGTV HD",
        "acc": "N",
        "value": 10
    },
    {
        "ch": "POLSAT Café HD",
        "acc": "JM",
        "value": 2
    },
    {
        "ch": "TVP ABC",
        "acc": "N",
        "value": 8
    },
    {
        "ch": "METRO",
        "acc": "N",
        "value": 4
    },
    {
        "ch": "TVP Kultura",
        "acc": "JM",
        "value": 2
    },
    {
        "ch": "TV Puls",
        "acc": "N",
        "value": 11
    },
    {
        "ch": "POLSAT 2",
        "acc": "AD",
        "value": 11
    },
    {
        "ch": "TVP Dokument",
        "acc": "JM",
        "value": 1
    },
    {
        "ch": "TVP Sport",
        "acc": "JM",
        "value": 2
    },
    {
        "ch": "HGTV HD",
        "acc": "AD",
        "value": 5
    },
    {
        "ch": "MiniMini+ HD",
        "acc": "AD",
        "value": 14
    },
    {
        "ch": "TVP Historia",
        "acc": "N",
        "value": 11
    },
    {
        "ch": "TVP 2",
        "acc": "N",
        "value": 7
    },
    {
        "ch": "TVN 7",
        "acc": "AD",
        "value": 3
    },
    {
        "ch": "TVP 3",
        "acc": "JM",
        "value": 5
    },
    {
        "ch": "TV 6",
        "acc": "N",
        "value": 10
    },
    {
        "ch": "TVP Seriale",
        "acc": "N",
        "value": 26
    },
    {
        "ch": "TVP ABC",
        "acc": "JM",
        "value": 6
    },
    {
        "ch": "POLSAT Rodzina",
        "acc": "JM",
        "value": 2
    },
    {
        "ch": "TVS",
        "acc": "AD",
        "value": 1
    },
    {
        "ch": "TVP 1",
        "acc": "AD",
        "value": 1
    },
    {
        "ch": "TVN Turbo HD",
        "acc": "N",
        "value": 4
    }
];
